#!/bin/bash

#mkdir -p ~/Publish/Hosts
#cd ~/Publish/Hosts
cd "$(dirname "$0")"
pwd
git fetch --all
git reset --hard origin/master
git pull

echo `date +"### 开始更新 %Y-%m-%d %H:%M:%S"` > Hosts_All.txt
echo ` ` >> Hosts_All.txt

# echo "***下载 g hosts 文件***"
# wget --no-check-certificate https://151.101.56.133/googlehosts/hosts/master/hosts-files/hosts -O hosts_g;
# echo "***下载 ad hosts 文件***"
# wget --no-check-certificate https://raw.githubusercontent.com/VeleSila/yhosts/master/hosts -O hosts_ad;
# echo "***合并 hosts 文件***"
# cat hosts_g hosts_ad >> Hosts_All.txt
# rm hosts_g hosts_ad
# echo ` ` >> Hosts_All.txt
# echo `date +"### 完成更新 %Y-%m-%d %H:%M:%S"` >> Hosts_All.txt

echo "***下载 domains 文件***"
wget https://anti-ad.net/domains.txt -O domains;
sed -i '/^[^#]/s/^/'"0.0.0.0 "'/' domains
echo "***合并 hosts 文件***"
cat domains MI_Hosts.txt >> Hosts_All.txt
echo ` ` >> Hosts_All.txt
echo `date +"### 完成更新 %Y-%m-%d %H:%M:%S"` >> Hosts_All.txt

echo "***复制 hosts 文件***"
cat Hosts_All.txt | base64 > HB

echo "***hosts 文件更新完成"
git add . && git commit -m "更新hosts" && git push